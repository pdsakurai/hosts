# Hosts files

## Blocklist for fake news in the Philippines
The initial content of the [blocklist][hosts-fakenews] is based on [Wikipedia](https://en.wikipedia.org/wiki/List_of_fake_news_websites#For_Philippine_audiences)'s list.
The [blocklist][hosts-fakenews] can be used in solutions that accept a list in hosts file format (such as [Pi-hole]). [It][hosts-fakenews] can be accessed through this URL:
>  `https://kutt.it/blocklist-fakenews-ph`

In case you want to add a new domain that is not yet in the [blocklist][hosts-fakenews], follow [the instructions for requesting new domains or websites for blocklisting][RequestNewDomain].

Please make sure that you that you have already done [the checklist for spotting fake news][fakenews-checklist] before requesting.

### Requesting to add new domain in the blocklist
In case you want to report a new domain or website that is not yet in the blocklist, do the following:
1. Check first if there is an existing open issue posted already in the [[Issues] page][IssuesPage] by searching for the domain or website you are about to request.
2. If there is an existing open issue already, visit it and follow [the instruction for adding supporting references to an existing request][AddSupportingReferences].
3. Otherwise, follow [the instruction for creating a request to add a new domain or website][CreateNewRequest].

### Adding supporting references to an existing request
Once you are on the open issue for an existing request, do the following:
1. Post supporting references (such as screenshots or links to their contents) showing that the website is spreading fake news.
2. Click the [Comment] button.

### Creating request to add new domain in the blocklist
In case there's no existing open issue yet for the new domain or website that you are about to report, do the following:
1. Create a new issue in the [[Issues] page][IssuesPage].
2. Complete the [New Issue] form by filling the following required fields:
	- **Title** - The domain or website (such as `fakenews.com` and `www.fakenews.com`) you are about to request to be added into the blacklist.
	- **Description** - Supporting references (such as screenshots or links to their contents) showing that the website is spreading fake news. 
3. Finally click the [Submit issue] button.

[AddSupportingReferences]: #adding-supporting-references-to-an-existing-request
[CreateNewRequest]: #creating-request-to-add-new-domain-in-the-blocklist
[RequestNewDomain]: #requesting-to-add-new-domain-in-the-blocklist
[IssuesPage]: https://gitlab.com/pdsakurai/hosts/-/issues
[hosts-fakenews]: https://kutt.it/blocklist-fakenews-ph
[Pi-hole]: http://pi-hole.net
[fakenews-checklist]: http://blogs.ifla.org/lpa/files/2017/01/How-to-Spot-Fake-News-1.jpg